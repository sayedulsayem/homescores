<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'homescores');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'aS-,}Ys,;S*wbLdh%V:4m>#K2t~BSH?DauF+BpAt_@08Dlw;WQr*[?7[wt{yWO;<');
define('SECURE_AUTH_KEY',  '7v=r$*Jic/V0Bn]TIu:,q<4[q<B.-)TJx`zQZsxYWL]D5AGnY<mM#`;1(9v&o<`B');
define('LOGGED_IN_KEY',    '0<XXyYjM=c)a6ZOj3Y_{WDb,9aZ^9S#nr)JBLk%[Xcm+eHO;5VL>)BZ`_LEuapwR');
define('NONCE_KEY',        'JJG>5IOi1P5}_2u DLkE%LfV<<v{DpxRf*n%%jv(4d}?g<H=}8:mF*{Oen-@}p/2');
define('AUTH_SALT',        'od!6maZ9Secu@sG}jJ|Mmxw.J`sj]LzE?hF9#Cs)+6&E5-X~F[|hmq747Gm$kxTK');
define('SECURE_AUTH_SALT', 'oQh*Y-;BufncrCu(4Y05OanvXjZNIiuOM8J31|=59&mx7Q|1z.^f}F)K Kp|KYhQ');
define('LOGGED_IN_SALT',   'iLf^.#B!U<5Y2&/=Ve]BmH{7qrCOsiq6fCr2AfU]B|p$1[`j:Ul5U 4Z) NGtQHP');
define('NONCE_SALT',       '%mRe:vZjBq/.4{Q.`oNQ-/SLb CQchN64>C(@rGb8V1 F.JpkF5`]X!Tg(Ish?r,');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
